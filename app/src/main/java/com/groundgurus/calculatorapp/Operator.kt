package com.groundgurus.calculatorapp

enum class Operator {
    ADD, SUBTRACT, MULTIPLY, DIVIDE, NONE
}