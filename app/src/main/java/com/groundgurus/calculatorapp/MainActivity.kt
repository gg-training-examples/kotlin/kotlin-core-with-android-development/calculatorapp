package com.groundgurus.calculatorapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

// 111 + 123 * 123 - 123

class MainActivity : AppCompatActivity() {
    var equation: String = ""
    var operator: Operator = Operator.NONE
    lateinit var outputTextView: TextView
    var numbers = mutableListOf<Pair<Int, Operator>>()
    var operatorClick: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val oneButton = findViewById<Button>(R.id.oneButton)
        val addButton = findViewById<Button>(R.id.addButton)
        val equalsButton = findViewById<Button>(R.id.equalsButton)
        outputTextView = findViewById(R.id.outputTextView)

        oneButton.setOnClickListener {
            if (operatorClick) {
                equation = "1"
            } else {
                equation += "1"
            }

            outputTextView.text = equation

            operatorClick = false
        }

        addButton.setOnClickListener {
            operator = Operator.ADD
            operatorClick = true
            numbers.add(Pair(equation.toInt(), Operator.ADD))
        }

        equalsButton.setOnClickListener {
            numbers.add(Pair(equation.toInt(), Operator.NONE))
            calculate()
        }
    }

    private fun calculate() {
        var result = 0

        numbers.forEach { number ->
            when (operator) {
                Operator.ADD -> result += number.first
                else -> TODO("Do something here")
            }
        }

        outputTextView.text = result.toString()
        numbers = mutableListOf()
        numbers.add(Pair(result, Operator.NONE))
        equation = result.toString()
    }
}
